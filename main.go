package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"test_subscriber/util"

	"cloud.google.com/go/pubsub"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	ctx := context.Background()
	proj := "dataintegration-072018"
	if proj == "" {
		fmt.Fprintf(os.Stderr, "GOOGLE_CLOUD_PROJECT environment variable must be set.\n")
		os.Exit(1)
	}
	client, err := pubsub.NewClient(ctx, proj)
	if err != nil {
		log.Fatalf("Could not create pubsub Client: %v", err)
	}

	// Print all the subscriptions in the project.
	fmt.Println("Listing all subscriptions from the project:")
	subs, err := util.List(client)
	if err != nil {
		log.Fatal(err)
	}
	for _, sub := range subs {
		fmt.Println(sub)
	}

	t := util.CreateTopicIfNotExists(client)

	const sub = "transfashion-topic-sub"
	// Create a new subscription.
	if err := util.Create(client, sub, t); err != nil {
		log.Print(err)
	}

	// Pull messages via the subscription.
	if err := util.PullMsgs(client, sub, t); err != nil {
		log.Fatal(err)
	}
	// return err
}
